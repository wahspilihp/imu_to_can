# Setup
1. Install the SeeedStudio CAN-HAT drivers as specified [here](https://wiki.seeedstudio.com/2-Channel-CAN-BUS-FD-Shield-for-Raspberry-Pi/#software).
1. Install [python-can](https://python-can.readthedocs.io/en/master/) using pip
1. Install [MSCL](https://github.com/LORD-MicroStrain/MSCL#downloads)

